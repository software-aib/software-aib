import colors from "vuetify/es5/util/colors"

export default {
    // Target: https://go.nuxtjs.dev/config-target
    target: "static",

    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        titleTemplate: "%s - team-blog",
        title: "team-blog",
        htmlAttrs: {
            lang: "en",
        },
        meta: [
            { charset: "utf-8" },
            {
                name: "viewport",
                content: "width=device-width, initial-scale=1",
            },
            { hid: "description", name: "description", content: "" },
            { name: "format-detection", content: "telephone=no" },
        ],
        link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: ["~assets/global.scss"],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [{ src: "~/plugins/vuelidate" }],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [
        // https://go.nuxtjs.dev/eslint
        "@nuxtjs/eslint-module",
        // https://go.nuxtjs.dev/vuetify
        "@nuxtjs/vuetify",
        "@nuxtjs/firebase",
    ],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [],
    firebase: {
        config: {
            apiKey: "AIzaSyBPTwW0iacxrLfqvaedoIBYeyHQYCnq0FQ",
            authDomain: "team-blog-59533.firebaseapp.com",
            projectId: "team-blog-59533",
            storageBucket: "team-blog-59533.appspot.com",
            messagingSenderId: "334958863009",
            appId: "1:334958863009:web:723b07d5869139c06586e0",
            databaseURL: "https://team-blog-59533-default-rtdb.firebaseio.com",
            measurementId: "",
        },
        services: {
            auth: {
                initialize: {
                    onAuthStateChangedAction: "onAuthStateChanged",
                },
                ssr: true,
            },
            storage: true,
            database: true,
        },
    },
    env: {
        firebaseAPIKey: "AIzaSyBPTwW0iacxrLfqvaedoIBYeyHQYCnq0FQ",
    },

    // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
    vuetify: {
        customVariables: ["~/assets/variables.scss"],
        theme: {
            light: true,
            themes: {
                light: {
                    primary: colors.blue.lighten2,
                    accent: colors.grey.lighten3,
                    secondary: colors.amber.lighten3,
                    info: colors.teal.lighten1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.accent3,
                },
                dark: {
                    primary: colors.blue.darken2,
                    accent: colors.grey.darken3,
                    secondary: colors.amber.darken3,
                    info: colors.teal.darken1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.accent3,
                },
            },
        },
    },

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
        babel: {
            plugins: [["babel-plugin-istanbul"]],
        },
    },
}
