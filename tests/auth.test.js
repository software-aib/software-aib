import LoginPage from "@/pages/auth/login.vue"
import RegisterPage from "@/pages/auth/register.vue"
import {
    addHelpers,
    addVuetify,
    addVuex,
    bootstrapVueContext,
    compositeConfiguration,
} from "~/tests/utils"

describe("Index page of the website", () => {
    let vueContext = null

    beforeEach(() => {
        vueContext = bootstrapVueContext(
            compositeConfiguration(addVuex, addVuetify, addHelpers())
        )
    })

    afterEach(() => {
        vueContext.teardownVueContext()
    })

    test("Test Login Page", () => {
        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.mount(LoginPage, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            stubs: {
                NuxtLink: true,
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: getDownloadURL(),
                    },
                    database: {
                        ref: update(),
                    },
                },
            },
        })

        expect(wrapper.text()).toContain("Login to account")
    })

    test("Test Signup Page", () => {
        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.mount(RegisterPage, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            stubs: {
                NuxtLink: true,
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: getDownloadURL(),
                    },
                    database: {
                        ref: update(),
                    },
                },
            },
        })

        expect(wrapper.text()).toContain("Create an account")
    })
})
