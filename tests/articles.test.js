import "../store"
import Vuex from "vuex"
import { createLocalVue } from "@vue/test-utils"
import ArticlesPage from "@/pages/article/index.vue"
import FirstFullArticle from "@/pages/article/complex_id1.vue"
import LastFullArticle from "@/pages/article/complex_id2.vue"
import {
    addHelpers,
    addVuetify,
    addVuex,
    bootstrapVueContext,
    compositeConfiguration,
} from "~/tests/utils"

describe("Index page of the website", () => {
    let vueContext = null

    beforeEach(() => {
        vueContext = bootstrapVueContext(
            compositeConfiguration(addVuex, addVuetify, addHelpers())
        )
    })

    afterEach(() => {
        vueContext.teardownVueContext()
    })

    test("Test Profile Page", () => {
        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.mount(ArticlesPage, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            stubs: {
                NuxtLink: true,
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: getDownloadURL(),
                    },
                    database: {
                        ref: update(),
                    },
                },
            },
        })

        expect(wrapper.text()).toContain("Featured articles:")
    })

    test("Test Compose Article Page", () => {
        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const localVue = createLocalVue()
        localVue.use(Vuex)
        const store = new Vuex.Store({
            modules: {
                state: {
                    articles: [],
                    user: {
                        uid: "spyCu3JggEPRUAI2eJJMuH0bAXI3",
                        email: "oziomatest@gmail.com",
                    },
                    getters: {
                        isAuthenticated(state) {
                            let token = state.token
                            if (token || process.client) {
                                token = localStorage.getItem("jwtToken")
                            }
                            return token
                        },
                    },
                },
            },
        })

        const wrapper = vueContext.vueTestUtils.mount(FirstFullArticle, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            store,
            stubs: {
                NuxtLink: true,
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: getDownloadURL(),
                    },
                    database: {
                        ref: update(),
                    },
                },
            },
        })

        expect(wrapper.text()).toContain("Written by")
    })

    test("Test Compose Article Page", () => {
        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const localVue = createLocalVue()
        localVue.use(Vuex)
        const store = new Vuex.Store({
            modules: {
                state: {
                    articles: [],
                    user: {
                        uid: "spyCu3JggEPRUAI2eJJMuH0bAXI3",
                        email: "oziomatest@gmail.com",
                    },
                    getters: {
                        isAuthenticated(state) {
                            let token = state.token
                            if (token || process.client) {
                                token = localStorage.getItem("jwtToken")
                            }
                            return token
                        },
                    },
                },
            },
        })

        const wrapper = vueContext.vueTestUtils.mount(LastFullArticle, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            store,
            stubs: {
                NuxtLink: true,
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: getDownloadURL(),
                    },
                    database: {
                        ref: update(),
                    },
                },
            },
        })

        expect(wrapper.text()).toContain("Written by")
    })
})
