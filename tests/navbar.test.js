import "../store"
import Vuex from "vuex"
import { createLocalVue } from "@vue/test-utils"
import NavBar from "@/components/NavBar.vue"
import {
    addHelpers,
    addVuetify,
    addVuex,
    bootstrapVueContext,
    compositeConfiguration,
} from "~/tests/utils"

describe("Testing the NavBar component", () => {
    let vueContext = null

    beforeEach(() => {
        vueContext = bootstrapVueContext(
            compositeConfiguration(addVuex, addVuetify, addHelpers())
        )
    })

    afterEach(() => {
        vueContext.teardownVueContext()
    })

    // test("Text Components in NavBar", () => {
    //     const wrapper = mount(NavBar)

    //     expect(wrapper.html()).toContain("Home")
    //     expect(wrapper.html()).toContain("Articles")
    //     expect(wrapper.html()).toContain("Profile")
    // })
    test("Text Components in NavBar", () => {
        const localVue = createLocalVue()
        localVue.use(Vuex)
        const store = new Vuex.Store({
            modules: {
                moduleA: {
                    state: {},
                    getters: {
                        isAuthenticated(state) {
                            let token = state.token
                            if (token || process.client) {
                                token = localStorage.getItem("jwtToken")
                            }
                            return token
                        },
                    },
                },
            },
        })

        const wrapper = vueContext.vueTestUtils.shallowMount(NavBar, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            store,
        })

        expect(wrapper.text()).toContain("Articles")
        // expect(wrapper.text()).toContain("Profile")
        expect(wrapper.text()).toContain("Login")
    })
})
