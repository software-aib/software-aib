import ProfileHeader from "@/components/ProfileHeader.vue"
import {
    addHelpers,
    addVuetify,
    addVuex,
    bootstrapVueContext,
    compositeConfiguration,
} from "~/tests/utils"

describe("Profile header of the profile page", () => {
    let vueContext = null

    beforeEach(() => {
        vueContext = bootstrapVueContext(
            compositeConfiguration(addVuex, addVuetify, addHelpers())
        )
    })

    afterEach(() => {
        vueContext.teardownVueContext()
    })

    test("Render the profile information when passed", async () => {
        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.mount(ProfileHeader, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            propsData: {
                profileInfo: {
                    fullname: "Jane Doe",
                    bio: "Vestibulum dapibus sagittis odio, non vestibulum felis accumsan sit amet",
                    profileImage: "static/unknown_user_avatar.jpeg",
                    email: "",
                    username: "@janeskidoeI",
                },
            },
            stubs: {
                NuxtLink: true,
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: jest.fn(() => getDownloadURL()),
                    },
                    database: {
                        ref: jest.fn(() => update()),
                    },
                },
            },
        })

        expect(wrapper.text()).toContain("Jane Doe")
        try {
            await wrapper.vm.getAvatarURL(() => {
                expect(wrapper.vm.$data.avatar).toBe(
                    "static/unknown_user_avatar.jpeg"
                )
            })
        } catch (e) {
            await expect(wrapper.vm.getAvatarURL()).rejects.toThrow(e)
        }
    })

    test("Render the profile information when passed", async () => {
        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.mount(ProfileHeader, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            propsData: {
                profileInfo: {
                    fullname: "Jane Doe",
                    bio: "Vestibulum dapibus sagittis odio, non vestibulum felis accumsan sit amet",
                    email: "",
                    profileImage: "",
                    username: "@janeskidoeI",
                },
            },
            stubs: {
                NuxtLink: true,
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: jest.fn(() => getDownloadURL()),
                    },
                    database: {
                        ref: jest.fn(() => update()),
                    },
                },
            },
        })

        try {
            await wrapper.vm.getAvatarURL(() => {
                expect(wrapper.vm.$data.avatar).toBe(
                    "static/unknown_user_avatar.jpeg"
                )
            })
        } catch (e) {
            await expect(wrapper.vm.getAvatarURL()).rejects.toThrow(e)
        }
    })
})
