import ArticleCard from "@/components/ArticleCard.vue"
import HorizontalCard from "@/components/HorizontalCard.vue"
import HorizontalCardBig from "@/components/HorizontalCardBig.vue"
import {
    addHelpers,
    addVuetify,
    addVuex,
    bootstrapVueContext,
    compositeConfiguration,
} from "~/tests/utils"

describe("Article Cards", () => {
    let vueContext = null

    beforeEach(() => {
        vueContext = bootstrapVueContext(
            compositeConfiguration(addVuex, addVuetify, addHelpers())
        )
    })

    afterEach(() => {
        vueContext.teardownVueContext()
    })

    test("Test Article Card Component", () => {
        const wrapper = vueContext.vueTestUtils.shallowMount(ArticleCard, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            propsData: {
                articleInfo: {
                    title: "What is Unit Test?",
                    author: "Tester Jenn",
                    content:
                        "It is a long established fact that a reader will be distracted by the TESTABLE content of a page when looking at its layout. The point of using Lotest Testum is that",
                    createdAt: "Feb 31st 2022",
                    updatedAt: "",
                    comments: {},
                    imageLink: "",
                    id: "test_id",
                },
            },
            stubs: {
                NuxtLink: true,
            },
        })

        expect(wrapper.props().articleInfo.title).toBe("What is Unit Test?")
        expect(wrapper.props().articleInfo.author).toBe("Tester Jenn")
        expect(wrapper.props().articleInfo.createdAt).toBe("Feb 31st 2022")
        expect(wrapper.props().articleInfo.id).toBe("test_id")
        expect(wrapper.props().articleInfo.content).toBe(
            "It is a long established fact that a reader will be distracted by the TESTABLE content of a page when looking at its layout. The point of using Lotest Testum is that"
        )
    })

    test("Test Horizontal Card Component", () => {
        const wrapper = vueContext.vueTestUtils.shallowMount(HorizontalCard, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            propsData: {
                firstArticleInfo: {
                    title: "What is Unit Test?",
                    author: "Tester Jenn",
                    content:
                        "It is a long established fact that a reader will be distracted by the TESTABLE content of a page when looking at its layout. The point of using Lotest Testum is that",
                    createdAt: "Feb 31st 2022",
                    updatedAt: "",
                    comments: {},
                    imageLink: "",
                    id: "test_id",
                },
            },
            stubs: {
                NuxtLink: true,
            },
        })

        expect(wrapper.props().firstArticleInfo.title).toBe(
            "What is Unit Test?"
        )
        expect(wrapper.props().firstArticleInfo.author).toBe("Tester Jenn")
        expect(wrapper.props().firstArticleInfo.createdAt).toBe("Feb 31st 2022")
        expect(wrapper.props().firstArticleInfo.id).toBe("test_id")
        expect(wrapper.props().firstArticleInfo.content).toBe(
            "It is a long established fact that a reader will be distracted by the TESTABLE content of a page when looking at its layout. The point of using Lotest Testum is that"
        )
    })

    test("Test Big Horizontal Card Component", () => {
        const wrapper = vueContext.vueTestUtils.shallowMount(
            HorizontalCardBig,
            {
                localVue: vueContext.vue,
                vuetify: vueContext.vuetifyInstance,
                propsData: {
                    lastArticleInfo: {
                        title: "What is Unit Test?",
                        author: "Tester Jenn",
                        content:
                            "It is a long established fact that a reader will be distracted by the TESTABLE content of a page when looking at its layout. The point of using Lotest Testum is that",
                        createdAt: "Feb 31st 2022",
                        updatedAt: "",
                        comments: {},
                        imageLink: "",
                        id: "test_id",
                    },
                },
                stubs: {
                    NuxtLink: true,
                },
            }
        )

        expect(wrapper.props().lastArticleInfo.title).toBe("What is Unit Test?")
        expect(wrapper.props().lastArticleInfo.author).toBe("Tester Jenn")
        expect(wrapper.props().lastArticleInfo.createdAt).toBe("Feb 31st 2022")
        expect(wrapper.props().lastArticleInfo.id).toBe("test_id")
        expect(wrapper.props().lastArticleInfo.content).toBe(
            "It is a long established fact that a reader will be distracted by the TESTABLE content of a page when looking at its layout. The point of using Lotest Testum is that"
        )
    })
})
