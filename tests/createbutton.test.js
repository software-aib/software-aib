import CreateButton from "@/components/CreateButton.vue"
import {
    addHelpers,
    addVuetify,
    addVuex,
    bootstrapVueContext,
    compositeConfiguration,
} from "~/tests/utils"

describe("Create Post", () => {
    let vueContext = null

    beforeEach(() => {
        vueContext = bootstrapVueContext(
            compositeConfiguration(addVuex, addVuetify, addHelpers())
        )
    })

    afterEach(() => {
        vueContext.teardownVueContext()
    })

    test("Test Create Post Button Component", () => {
        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.shallowMount(CreateButton, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            mocks: {
                $fire: {
                    storage: {
                        ref: getDownloadURL(),
                    },
                    database: {
                        ref: update(),
                    },
                },
            },
        })

        const createbutton = wrapper.find('[data-testid="createbutton"]')

        expect(createbutton.exists()).toBe(true)
    })
})
