import OldFullArticle from "@/components/Fullarticle.vue"
import {
    addHelpers,
    addVuetify,
    addVuex,
    bootstrapVueContext,
    compositeConfiguration,
} from "~/tests/utils"

describe("Fullarticle", () => {
    let vueContext = null

    beforeEach(() => {
        vueContext = bootstrapVueContext(
            compositeConfiguration(addVuex, addVuetify, addHelpers())
        )
    })

    afterEach(() => {
        vueContext.teardownVueContext()
    })

    test("Render the full article when passed", () => {
        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.mount(OldFullArticle, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            propsData: {
                fullArticleInfo: {
                    title: "Test title",
                    author: "Jane Doe",
                    comments: {},
                    content:
                        "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that",
                    createdAt: "May 20th 2020",
                    updatedAt: "",
                    id: "complextest_id",
                    imageLink: "",
                },
            },
            stubs: {
                NuxtLink: true,
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: getDownloadURL(),
                    },
                    database: {
                        ref: update(),
                    },
                },
            },
        })

        expect(wrapper.text()).toContain("Test title")
    })
})
