import "../store"
import Vuex from "vuex"
import { createLocalVue } from "@vue/test-utils"
import AdminImageUploader from "@/components/ImageUploader.vue"
import {
    addHelpers,
    addVuetify,
    addVuex,
    bootstrapVueContext,
    compositeConfiguration,
} from "~/tests/utils"

describe("ImageUploader", () => {
    let vueContext = null
    let file

    beforeEach(() => {
        vueContext = bootstrapVueContext(
            compositeConfiguration(addVuex, addVuetify, addHelpers())
        )
        file = new File(["(⌐□_□)"], "sqrs.png", { type: "image/png" })
    })

    afterEach(() => {
        vueContext.teardownVueContext()
    })

    test("Test ImageUploader Component", async () => {
        const localVue = createLocalVue()
        localVue.use(Vuex)
        const store = new Vuex.Store({
            modules: {
                moduleA: {
                    state: {},
                    getters: {
                        isAuthenticated(state) {
                            let token = state.token
                            if (token || process.client) {
                                token = localStorage.getItem("jwtToken")
                            }
                            return token
                        },
                    },
                },
            },
        })

        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.shallowMount(
            AdminImageUploader,
            {
                localVue: vueContext.vue,
                vuetify: vueContext.vuetifyInstance,
                store,
                propsData: {
                    value: {},
                },
                mocks: {
                    $fire: {
                        storage: {
                            ref: getDownloadURL(),
                        },
                        database: {
                            ref: update(),
                        },
                    },
                },
            }
        )

        const uploader = wrapper.find('[data-testid="fileUploader"]')
        await uploader.trigger("input", { file })

        const image = document.getElementsByTagName("image-uploader")
        expect(image.item.name).toBe("item")
    })
})
