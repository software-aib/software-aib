import ProfilePage from "@/pages/profile/index.vue"
import ComposeArticle from "@/pages/profile/compose.vue"
import {
    addHelpers,
    addVuetify,
    addVuex,
    bootstrapVueContext,
    compositeConfiguration,
} from "~/tests/utils"

describe("Index page of the website", () => {
    let vueContext = null

    beforeEach(() => {
        vueContext = bootstrapVueContext(
            compositeConfiguration(addVuex, addVuetify, addHelpers())
        )
    })

    afterEach(() => {
        vueContext.teardownVueContext()
    })

    test("Test Profile Page", () => {
        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.mount(ProfilePage, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            stubs: {
                NuxtLink: true,
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: getDownloadURL(),
                    },
                    database: {
                        ref: update(),
                    },
                },
            },
        })

        expect(wrapper.text()).toContain("Compose Article")
    })

    test("Test Compose Article Page", () => {
        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.mount(ComposeArticle, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            stubs: {
                NuxtLink: true,
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: getDownloadURL(),
                    },
                    database: {
                        ref: update(),
                    },
                },
            },
        })

        expect(wrapper.text()).toContain("Compose Article")
    })
})
