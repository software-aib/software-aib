import "../store"
import Vuex from "vuex"
import { createLocalVue } from "@vue/test-utils"
import AddComment from "@/components/AddComment.vue"
import Comment from "@/components/Comment.vue"
import "@testing-library/jest-dom"
import {
    addHelpers,
    addVuetify,
    addVuex,
    bootstrapVueContext,
    compositeConfiguration,
} from "~/tests/utils"

describe("Comment", () => {
    let vueContext = null

    const reloadFn = () => {
        location.reload(true)
    }

    beforeEach(() => {
        vueContext = bootstrapVueContext(
            compositeConfiguration(addVuex, addVuetify, addHelpers())
        )
        Object.defineProperty(window, "location", {
            configurable: true,
            value: { reload: jest.fn() },
        })
    })

    afterEach(() => {
        vueContext.teardownVueContext()
        Object.defineProperty(window, "location", {
            configurable: true,
            value: location,
        })
    })

    test("Test Comment Component", () => {
        const localVue = createLocalVue()
        localVue.use(Vuex)
        const store = new Vuex.Store({
            modules: {
                state: {
                    articles: [],
                    user: {
                        uid: "spyCu3JggEPRUAI2eJJMuH0bAXI3",
                        email: "oziomatest@gmail.com",
                    },
                    getters: {
                        isAuthenticated(state) {
                            let token = state.token
                            if (token || process.client) {
                                token = localStorage.getItem("jwtToken")
                            }
                            return token
                        },
                    },
                },
            },
        })

        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.shallowMount(Comment, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            store,
            propsData: {
                commentInfo: {
                    author: "Tester Queue",
                    content:
                        "Turns out that if this test passes, the comment works more or less.",
                    createdAt: "Feb 31st 2022",
                },
                articleId: "complextest_id9289we",
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: getDownloadURL(),
                    },
                    database: {
                        ref: update(),
                    },
                },
            },
        })

        expect(wrapper.props().commentInfo.author).toBe("Tester Queue")
        expect(wrapper.props().commentInfo.createdAt).toBe("Feb 31st 2022")
        expect(wrapper.props().commentInfo.content).toBe(
            "Turns out that if this test passes, the comment works more or less."
        )
    })

    // test("Test Delete Comment Component", () => {
    //     const wrapper = vueContext.vueTestUtils.mount(Comment, {
    //         localVue: vueContext.vue,
    //         vuetify: vueContext.vuetifyInstance,
    //         propsData: {
    //             commentInfo: {
    //                 author: "Tester Queue",
    //                 content:
    //                     "Turns out that if this test passes, the comment works more or less.",
    //                 createdAt: "Feb 31st 2022",
    //                 updatedAt: "",
    //                 imageLink: "",
    //                 id: "commenttest_id",
    //             },
    //             articleId: "complextest_id",
    //         },
    //     })

    //     const deletecommentbutton = wrapper.find(
    //         '[data-testid="deletecommentbutton"]'
    //     )

    //     expect(deletecommentbutton.exists()).toBe(true)
    //     expect(wrapper.vm.$data.deletedId).toBe(false)
    //     deletecommentbutton.vm.$emit("click")

    //     wrapper.vm.$nextTick(() => {
    //         expect(wrapper.vm.$data.deletedId).toBe(true)
    //     })
    // })

    // jest.setTimeout(30000)
    test("Test Delete Comment Method", async () => {
        const localVue = createLocalVue()
        localVue.use(Vuex)
        const store = new Vuex.Store({
            modules: {
                state: {
                    articles: [],
                    user: {
                        uid: "spyCu3JggEPRUAI2eJJMuH0bAXI3",
                        email: "oziomatest@gmail.com",
                    },
                    getters: {
                        isAuthenticated(state) {
                            let token = state.token
                            if (token || process.client) {
                                token = localStorage.getItem("jwtToken")
                            }
                            return token
                        },
                    },
                },
            },
        })

        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.shallowMount(Comment, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            store,
            propsData: {
                commentInfo: {
                    author: "Tester Queue",
                    content:
                        "Turns out that if this test passes, the comment works more or less.",
                    createdAt: "Feb 31st 2022",
                },
                articleId: "complextest_id9289we",
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: getDownloadURL(),
                    },
                    database: {
                        ref: update(),
                    },
                },
            },
        })

        try {
            await wrapper.vm.deleteCommentMethod(() => {})
            reloadFn() // as defined above..
            expect(location.reload).toHaveBeenCalled()
        } catch (e) {
            await expect(wrapper.vm.deleteCommentMethod()).rejects.toThrow(e)
        }
    })

    test("Test Add Comment Submit Method", async () => {
        const localVue = createLocalVue()
        localVue.use(Vuex)
        const store = new Vuex.Store({
            modules: {
                state: {
                    articles: [],
                    user: {
                        uid: "spyCu3JggEPRUAI2eJJMuH0bAXI3",
                        email: "oziomatest@gmail.com",
                    },
                    getters: {
                        isAuthenticated(state) {
                            let token = state.token
                            if (token || process.client) {
                                token = localStorage.getItem("jwtToken")
                            }
                            return token
                        },
                    },
                },
            },
        })

        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.shallowMount(AddComment, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            store,
            propsData: {
                articleId: "complextest_id9289we",
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: getDownloadURL(),
                    },
                    database: {
                        ref: update(),
                    },
                },
            },
        })

        try {
            await wrapper.vm.submitCommentMethod(() => {
                expect(wrapper.text()).toBe(true)
            })
            reloadFn() // as defined above..
            expect(location.reload).toHaveBeenCalled()
        } catch (e) {
            await expect(wrapper.vm.submitCommentMethod()).rejects.toThrow(e)
        }
    })

    test("Test Add Comment Component", async () => {
        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.mount(AddComment, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            propsData: {
                articleId: "complextest_id9289we",
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: getDownloadURL(),
                    },
                    database: {
                        ref: update(),
                    },
                },
            },
        })

        const addcommentbutton = wrapper.find(
            '[data-testid="addcommentbutton"]'
        )

        expect(addcommentbutton.exists()).toBe(true)
        expect(wrapper.vm.$data.showTextBox).toBe(false)
        addcommentbutton.vm.$emit("click")

        await wrapper.vm.$nextTick()
        expect(wrapper.vm.$data.showTextBox).toBe(true)
    })

    test("Test Add Comment Text Box Component", async () => {
        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.mount(AddComment, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            propsData: {
                articleId: "complextest_id9289we",
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: getDownloadURL(),
                    },
                    database: {
                        ref: update(),
                    },
                },
            },
        })

        const addcommentbutton = wrapper.find(
            '[data-testid="addcommentbutton"]'
        )

        const commenttextbox = wrapper.find('[data-testid="commenttextbox"]')

        expect(commenttextbox.exists()).toBe(true)
        expect(commenttextbox.attributes().style).toBe(
            "width: 100%; display: none;"
        )
        addcommentbutton.vm.$emit("click")

        await wrapper.vm.$nextTick()
        expect(commenttextbox.attributes().style).toBe("width: 100%;")
    })
})
