import { shallowMount } from "@vue/test-utils"
import FooterBar from "@/components/FooterBar.vue"
import {
    addHelpers,
    addVuetify,
    addVuex,
    bootstrapVueContext,
    compositeConfiguration,
} from "~/tests/utils"

describe("Testing the NavBar component", () => {
    let vueContext = null

    beforeEach(() => {
        vueContext = bootstrapVueContext(
            compositeConfiguration(addVuex, addVuetify, addHelpers())
        )
    })

    afterEach(() => {
        vueContext.teardownVueContext()
    })

    test("Text Components in NavBar", () => {
        const wrapper = shallowMount(FooterBar)

        expect(wrapper.html()).toContain("hotcoffee")
    })
})
