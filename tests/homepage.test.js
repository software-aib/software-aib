import IndexPage from "@/pages/index.vue"
import {
    addHelpers,
    addVuetify,
    addVuex,
    bootstrapVueContext,
    compositeConfiguration,
} from "~/tests/utils"

describe("Index page of the website", () => {
    let vueContext = null

    beforeEach(() => {
        vueContext = bootstrapVueContext(
            compositeConfiguration(addVuex, addVuetify, addHelpers())
        )
    })

    afterEach(() => {
        vueContext.teardownVueContext()
    })

    test("Test Home Page", () => {
        const getDownloadURL = jest.fn()
        const update = jest.fn()

        const wrapper = vueContext.vueTestUtils.mount(IndexPage, {
            localVue: vueContext.vue,
            vuetify: vueContext.vuetifyInstance,
            stubs: {
                NuxtLink: true,
            },
            mocks: {
                $fire: {
                    storage: {
                        ref: getDownloadURL(),
                    },
                    database: {
                        ref: update(),
                    },
                },
            },
        })

        expect(wrapper.text()).toContain(
            "Software Quality Reliability and Security"
        )
    })

    // test("Test Home Page Fetch", async () => {
    //     const wrapper = vueContext.vueTestUtils.shallowMount(IndexPage, {
    //         localVue: vueContext.vue,
    //         vuetify: vueContext.vuetifyInstance,
    //     })

    //     await wrapper.vm.asyncData
    // })
})
