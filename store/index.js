export const state = () => ({
    articles: [],
    user: null,
})

export const mutations = {
    SET_ARTICLES(state, articles) {
        state.articles = articles
    },
    ADD_ARTICLE(state, article) {
        state.articles.push(article)
    },
    UPDATE_ARTICLE(state, upDateArticle) {
        for (let article of state.articles) {
            if (article.id === upDateArticle.id) {
                article = upDateArticle
            }
        }
    },
    CLEAR_USER(state) {
        state.user = null
    },
    SET_USER(state, userObject) {
        const { uid, email } = userObject
        state.user = { uid, email }
    },
}

export const actions = {
    async nuxtServerInit(vuexContext, serverContext) {
        if (
            serverContext.res &&
            serverContext.res.locals &&
            serverContext.res.locals.user
        ) {
            const { allClaims: claims, ...authUser } =
                serverContext.res.locals.user

            console.info(
                "Auth User verified on server-side. User: ",
                authUser,
                "Claims:",
                claims
            )

            await vuexContext.dispatch("onAuthStateChanged", { authUser })
        }
        const articlesRef = this.$fire.database.ref("articles")

        try {
            const snapshot = await articlesRef.once("value")
            const response = snapshot.val()
            const result = []
            for (const key in response) {
                result.push({ ...response[key], id: key })
            }
            vuexContext.commit("SET_ARTICLES", result)
        } catch (errorObject) {
            serverContext.error(errorObject)
        }
    },
    async onAuthStateChanged({ commit }, { authUser }) {
        if (!authUser) {
            commit("CLEAR_USER")
            return
        }
        if (authUser && authUser.getIdToken) {
            try {
                await authUser.getIdToken(true)
                // console.info("idToken", idToken)
            } catch (e) {
                console.error(e)
            }
        }
        commit("SET_USER", authUser)
    },
    // async updateArticle({ commit, state }, { articleId, article }) {
    //     const articleRef = this.$fire.database.ref(`articles/${articleId}`)

    //     try {
    //         const response = await articleRef.set({
    //             ...article,
    //         })
    //         console.log(response)
    //         commit("UPDATE_POST", { ...response, id: articleId })
    //     } catch (e) {
    //         console.error(e)
    //     }
    // },
    setArticles({ commit }, articles) {
        // console.log(articles)
        commit("SET_ARTICLES", articles)
    },
    // async addArticle({ commit, state }, article) {
    //     const articlesRef = this.$fire.database.ref("articles")

    //     try {
    //         const response = await articlesRef.set({
    //             ...article,
    //             updatedDate: new Date(),
    //         })
    //         console.log(response)
    //         commit("ADD_ARTICLE", response)
    //     } catch (e) {
    //         console.error(e)
    //     }
    // },
    async logIn({ commit, dispatch }, userInfo) {
        try {
            const response = await this.$fire.auth.signInWithEmailAndPassword(
                userInfo.email,
                userInfo.password
            )
            console.log(response.user)
        } catch (e) {
            console.error(e)
            throw new Error(e)
        }
    },
    async logOut({ commit, dispatch }) {
        try {
            await this.$fire.auth.signOut()
        } catch (e) {
            alert(e)
        }
    },
    async registerUser({ commit }, userInfo) {
        const { username, fullname, email, password, bioinfo, profileImage } =
            userInfo

        try {
            const createAuthenticatedUser =
                await this.$fire.auth.createUserWithEmailAndPassword(
                    email,
                    password
                )
            console.log(createAuthenticatedUser)
            const userId = createAuthenticatedUser.user.uid
            const userRef = this.$fire.database.ref(`users/${userId}`)

            await userRef.set({
                username,
                fullname,
                email,
                bioinfo,
                profileImage,
            })
        } catch (e) {
            console.error(e)
            throw new Error(e)
        }
    },
}

export const getters = {
    isAuthenticated(state) {
        try {
            return state.user.uid !== null
        } catch {
            return false
        }
    },
}
