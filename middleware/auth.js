export default function ({ store, redirect }) {
    if (!store.getters.isAuthenticated && process.client) {
        redirect("/auth/login")
    }
}
