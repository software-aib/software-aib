import { mutations, getters } from "../../store"

describe("The application has store", () => {
    it("should ensure the store is exposed to cypress", () => {
        cy.visit("/")
        cy.window()
            .its("$nuxt")
            .then((nuxt) => {
                console.dir(nuxt.$store)
            })
    })
})

describe("mutations", () => {
    context("ARTICLES", () => {
        const {
            SET_ARTICLES,
            ADD_ARTICLE,
            // UPDATE_ARTICLE,
            CLEAR_USER,
            SET_USER,
        } = mutations
        const articles = [
            {
                title: "What is Unit Test?",
                author: "Tester Jenn",
                content:
                    "It is a long established fact that a reader will be distracted by the TESTABLE content of a page when looking at its layout. The point of using Lotest Testum is that",
                createdAt: "Feb 31st 2022",
                updatedAt: "",
                comments: {},
                imageLink: "",
                id: "test_id",
            },
        ]
        const article = {
            title: "What is Unit Test?",
            author: "Tester Jenn",
            content:
                "It is a long established fact that a reader will be distracted by the TESTABLE content of a page when looking at its layout. The point of using Lotest Testum is that",
            createdAt: "Feb 31st 2022",
            updatedAt: "",
            comments: {},
            imageLink: "",
            id: "test_id",
        }
        it("should set the articles", () => {
            const state = { articles: [], user: null }
            SET_ARTICLES(state, articles)

            expect(state.articles.length).to.equal(1)
        })

        it("should add the article", () => {
            const state = { articles, token: "" }
            ADD_ARTICLE(state, article)
            expect(state.articles.length).to.equal(2)
        })

        it("should set the user", () => {
            const state = {
                articles: [],
                user: null,
            }
            const userObj = { uid: "user_id", email: "user@gmail.com" }
            SET_USER(state, userObj)
            expect(state.user.email).to.equal("user@gmail.com")
        })

        it("should clear the user", () => {
            const state = { articles: [], user: "" }
            CLEAR_USER(state)
            expect(state.user).to.equal(null)
        })
    })
})

// describe("actions", () => {
//     context("authentications", () => {
//         const payload = {
//             email: "oziomatest@gmail.com",
//             password: "sqrstest",
//             returnSecureToken: true,
//         }
//         const { logIn, logOut } = actions
//         it("should logIn the user", () => {
//             const state = { articles: [], token: "" }
//             logIn(payload)

//             expect(state.token).to.not.equal("")
//         })

//         it("should logOut the user", () => {
//             const state = { articles: [], token: "" }
//             logOut()
//             expect(state.token).to.equal(null)
//         })
//     })
// })

describe("getters", () => {
    context("isAuthenticated", () => {
        const { isAuthenticated } = getters
        it("should be authenticated", () => {
            const state = { articles: [], token: "ta4ka2na" }
            isAuthenticated(state)

            expect(state.token).to.equal("ta4ka2na")
        })

        it("should not be authenticated", () => {
            const state = { articles: [], token: null }
            isAuthenticated(state)
            expect(state.token).to.equal(null)
        })
    })
})

// describe("store actions", () => {
// const stateHolder = state()
// const payload = {
//     email: "oziomatest@gmail.com",
//     password: "sqrstest",
//     returnSecureToken: true,
// }
// beforeEach(() => {
//     stateHolder = state()
// })
// context("CLEAR_TOKEN", () => {
// it("should set then clear the auth token", () => {
// const getStore = () => cy.window().its("app.$store")
// mutations.SET_TOKEN(stateHolder, "ta4ka2na")
// expect(stateHolder.token, "changed value").to.equal("ta4ka2na")
// cy.wrap({ getStore }).its("state").should("exist")
// mutations.CLEAR_TOKEN(stateHolder)
// expect(stateHolder.token, "changed value").to.equal(null)
// })
// })
// context("logOut", () => {
// it("should log out the user", () => {
//     cy.wrap({ stateHolder }).then(actions.logIn(payload))
//     expect(stateHolder.token, "changed value").to.not.be.equal("")
//     cy.wrap({ stateHolder }).then(actions.logOut())
//     expect(stateHolder.token, "changed value").to.equal(null)
// })
// })
// context("isAuthenticated", () => {
// it("should check the authentication state", () => {
//     const stateHolder = state()
//     cy.wrap()
//         .then(getters.isAuthenticated(stateHolder))
//         .should("have.returned", "ta4ka2na")
// })
// })
// })
