describe("Post page for a logged in user", () => {
    beforeEach(() => {
        cy.visit("/auth/login")
        cy.get("[data-cy=auth-email]").type("oziomatest@gmail.com")
        cy.get("[data-cy=auth-password]").type("sqrstest")
        cy.get("[data-cy=auth-button]").click()
    })

    it("should visit the articles page find post elements", () => {
        cy.visit("/article/complex_id1")

        cy.get("h1").contains("What is our project about?")
        cy.get("h2.author").contains("Daniel, Marko, Nastya and Ozzie")
        cy.get("h2.date").contains("February 20th 2022")
    })

    it("should visit the articles page click on a card", () => {
        cy.visit("/article/complex_id1")

        cy.get(".articleCardReadMore").click({ multiple: true, force: true })
    })
})
