import faker from "@faker-js/faker"

describe("The Login/Signup Page", () => {
    it("should visit the auth page and find the log in elements", () => {
        cy.visit("/auth/login")
        cy.get("[data-cy=auth-email]").should("exist")
        cy.get("[data-cy=auth-password]").should("exist")
    })

    it("should visit the auth page and sign up", () => {
        cy.visit("/auth/register")
        cy.get("[data-cy=auth-username]").type("usertest")
        cy.get("[data-cy=auth-fullname]").type("User Test")
        cy.get("[data-cy=auth-email]").type(faker.internet.email())
        cy.get("[data-cy=auth-password]").type("usertest")
        cy.get("[data-cy=auth-bio]").type(
            "Testing the profile sign up elements"
        )
        cy.get("[data-cy=auth-button]").click()
        cy.get("[data-cy=auth-alert-success]").contains(
            "Account creation was successfully"
        )
    })

    it("should visit the auth page and fail to sign up", () => {
        cy.visit("/auth/register")
        cy.get("[data-cy=auth-email]").type("usertest@gmail.com")
        cy.get("[data-cy=auth-password]").type("usertest")
        cy.get("[data-cy=auth-button]").click()
        cy.get("[data-cy=auth-alert-error]").contains(
            "FirebaseError: Firebase: The email address is already in use by another account. (auth/email-already-in-use)."
        )
    })

    it("should visit the auth page and log in", () => {
        cy.visit("/auth/login")
        cy.get("[data-cy=auth-email]").type("usertest@gmail.com")
        cy.get("[data-cy=auth-password]").type("usertest")
        cy.get("[data-cy=auth-button]").click()
        cy.get("[data-cy=auth-alert-success]").contains(
            "Logged in successfully"
        )
    })

    it("should visit the auth page and fail log in: wrongly formatted email", () => {
        cy.visit("/auth/login")
        cy.get("[data-cy=auth-email]").type("usertest.com")
        cy.get("[data-cy=auth-password]").type("usertest")
        cy.get("[data-cy=auth-button]").click()
        cy.get("[data-cy=auth-alert-error]").contains(
            "FirebaseError: Firebase: The email address is badly formatted"
        )
    })

    it("should visit the auth page and fail log in: incorrect password", () => {
        cy.visit("/auth/login")
        cy.get("[data-cy=auth-email]").type("usertest@gmail.com")
        cy.get("[data-cy=auth-password]").type("use")
        cy.get("[data-cy=auth-button]").click()
        cy.get("[data-cy=auth-alert-error]").contains(
            "FirebaseError: Firebase: The password is invalid or the user does not have a password. (auth/wrong-password)."
        )
    })

    it("should visit the auth page and fail log in: no user", () => {
        cy.visit("/auth/login")
        cy.get("[data-cy=auth-email]").type("user@gmail.com")
        cy.get("[data-cy=auth-password]").type("use")
        cy.get("[data-cy=auth-button]").click()
        cy.get("[data-cy=auth-alert-error]").contains(
            "FirebaseError: Firebase: There is no user record corresponding to this identifier. The user may have been deleted."
        )
    })

    it("should log out a user", () => {
        cy.visit("/auth/login")
        cy.get("[data-cy=auth-email]").type("usertest@gmail.com")
        cy.get("[data-cy=auth-password]").type("usertest")
        cy.get("[data-cy=auth-button]").click()
        cy.url().should("include", "/")
        cy.get("[data-cy=auth-logout]").click()
        cy.get("[data-cy=auth-login]").should("exist")
    })
})
