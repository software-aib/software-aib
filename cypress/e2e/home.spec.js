describe("The Home Page", () => {
    it("should visit the home page and check the cards", () => {
        cy.visit("/")
        cy.get("[data-cy=home-card]").contains("Read more")
    })

    it("should visit the home page and check the title of the page", () => {
        cy.visit("/")
        cy.get("[data-cy=home-banner]").contains("Software Quality")
    })

    it("should visit the home page and find a navbar text", () => {
        cy.visit("/")
        cy.get("[data-cy=nav-articles]").contains("Articles")
    })

    it("should visit the home page and find footer elements", () => {
        cy.visit("/")
        cy.get("[data-cy=footer-text]").contains(
            "hotcoffee 2022 copyright all rights reserved"
        )
        cy.get("[data-cy=footer-icons]").should("be.visible")
    })

    it("should visit the home page and check that on click article page loads", () => {
        cy.visit("/")
        cy.window().then((w) => (w.beforeReload = true))
        cy.window().should("have.prop", "beforeReload", true)

        cy.get(".articleCardReadMore").click({ multiple: true, force: true })

        cy.window().should("not.have.prop", "beforeReload")
    })
})
