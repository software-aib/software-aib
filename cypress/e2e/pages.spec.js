describe("The Home Page", () => {
    it("should visit the home page", () => {
        cy.visit("/")
    })
})

describe("The Articles List Page", () => {
    it("should visit the articles page", () => {
        cy.visit("/article")
    })
    it("should visit the first main article page", () => {
        cy.visit("/article/complex_id1")
    })
    it("should visit the second main article page", () => {
        cy.visit("/article/complex_id2")
    })
})

describe("The Login Page", () => {
    it("should visit the login page", () => {
        cy.visit("/auth/login")
    })
})

describe("The Signup Page", () => {
    it("should visit the login page", () => {
        cy.visit("/auth/register")
    })
})

describe("The Profile Page", () => {
    it("should visit the profile page", () => {
        cy.visit("/profile")
    })
})

describe("The Editor Page", () => {
    it("should visit the profile page", () => {
        cy.visit("/profile/compose")
    })
})

describe("The Error Page", () => {
    it("should show the error layout", () => {
        cy.visit("/random", {
            failOnStatusCode: false,
        })
        cy.get("h1").contains("404")
    })
})
