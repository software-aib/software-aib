describe("Post page for a logged in user", () => {
    beforeEach(() => {
        cy.visit("/auth/login")
        cy.get("[data-cy=auth-email]").type("oziomatest@gmail.com")
        cy.get("[data-cy=auth-password]").type("sqrstest")
        cy.get("[data-cy=auth-button]").click()
    })

    it("should visit the articles page find create a comment", () => {
        cy.visit("/article")

        cy.get(".articleCardReadMore").click({ multiple: true, force: true })
        cy.get("[data-cy=add-comment]").should("be.visible")
        cy.get("[data-cy=comment-textbox]").should("not.be.visible")
    })

    it("should visit the articles page click create a comment", () => {
        cy.visit("/article")

        cy.get(".articleCardReadMore").click({ multiple: true, force: true })
        cy.get("[data-cy=add-comment]").click()
        cy.get("[data-cy=comment-textbox]").should("be.visible")
        cy.get("[data-cy=comment-textbox-title]").contains("Enter comment")
        cy.get("[data-cy=comment-textbox]").type("Test Comment before deadline")
        cy.get("[data-cy=add-comment-button]").click()
        cy.get("html").should("contain.text", "deadline")
    })

    it("should visit the articles page find delete a comment", () => {
        cy.visit("/article")

        cy.get(".articleCardReadMore").click({ multiple: true, force: true })
        cy.get("[data-cy=delete-comment]").should("exist")
    })

    it("should visit the articles page find a comment", () => {
        cy.visit("/article")

        cy.get(".articleCardReadMore").click({ multiple: true, force: true })
        cy.get("[data-cy=comment]").should("exist")
    })
})

describe("Post for a non logged in user", () => {
    it("should visit the articles page and not be able to create a comment", () => {
        cy.visit("/article")

        cy.get(".articleCardReadMore").click({ multiple: true, force: true })
        cy.get("[data-cy=add-comment]").should("not.exist")
    })

    it("should visit the articles page and not be able to delete a comment", () => {
        cy.visit("/article")

        cy.get(".articleCardReadMore").click({ multiple: true, force: true })
        cy.get("[data-cy=delete-comment]").should("not.exist")
    })

    it("should visit the articles page find a comment", () => {
        cy.visit("/article")

        cy.get(".articleCardReadMore").click({ multiple: true, force: true })
        cy.get("[data-cy=comment]").should("exist")
    })
})
