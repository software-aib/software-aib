describe("The Profile Page", () => {
    beforeEach(() => {
        cy.visit("/auth/login")
        cy.get("[data-cy=auth-email]").type("oziomatest@gmail.com")
        cy.get("[data-cy=auth-password]").type("sqrstest")
        cy.get("[data-cy=auth-button]").click()
        cy.url().should("include", "/")
        cy.visit("/profile")
    })

    it("should visit the articles page and check the cards", () => {
        cy.visit("/profile")
        cy.get("[data-cy=profile-card]").contains("Read more")
    })

    it("should visit the articles page and click the cards", () => {
        cy.visit("/profile")
        cy.get("[data-cy=profile-card]").click({ force: true })
        cy.get("html").contains("Written")
    })

    it("should visit the profile page and click compose article", () => {
        cy.visit("/profile")
        cy.get("[data-cy=compose-button]").click()
        cy.url().should("contain", "/profile/compose")
    })

    it("should visit the profile page and compose article", () => {
        cy.visit("/profile")
        cy.get("[data-cy=compose-button]").click()
        cy.get("[data-cy=compose-title").type("Cool testing skills")
        cy.get("[data-cy=compose-content]").type(
            "It was supposed to be a dream vacation. They had planned it over a year in advance so that it would be perfect in every way. It had been what they had been looking forward to through all the turmoil and negativity around them. It had been the light at the end of both their tunnels. Now that the dream vacation was only a week away, the virus had stopped all air travel.",
            { force: true }
        )
        cy.get("[data-cy=compose-action]").click({ force: true })
    })

    it("should visit the profile page and cancel compose article", () => {
        cy.visit("/profile")
        cy.get("[data-cy=compose-button]").click()
        cy.get("[data-cy=compose-cancel]").click({ force: true })
        cy.url().should("contain", "/profile")
    })

    it("should visit the profile page and find profile elements", () => {
        cy.visit("/profile")
        cy.get("[data-cy=profile-name]").should("exist")
        cy.get("[data-cy=profile-bio]").should("exist")
        cy.get("[data-cy=profile-photo]").should("exist")
    })

    it("should visit the profile page and find compose elements", () => {
        cy.visit("/profile/compose")
        cy.get("[data-cy=compose-title]").should("exist")
        cy.get("[data-cy=compose-content]").should("exist")
        cy.get("[data-cy=compose-image]").should("exist")
        cy.get("[data-cy=compose-action]").should("exist")
        cy.get("[data-cy=compose-cancel]").should("exist")
    })

    // it("Testing picture uploading", () => {
    //     cy.visit("/profile/compose")
    //     cy.get("[data-cy=compose-image]").selectFile({
    //         fileContents: ["(⌐□_□)"],
    //         fileName: "testPicture.png",
    //         mimeType: "image/png",
    //     })
    // })
})
