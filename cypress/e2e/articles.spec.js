describe("List of all articles", () => {
    beforeEach(() => {
        cy.visit("/article")
    })

    it("should visit the articles page and check the cards", () => {
        cy.get("[data-cy=articles-card]").contains("Read more")
    })

    it("should visit the articles page and check that on click article page loads", () => {
        cy.window().then((w) => (w.beforeReload = true))
        cy.window().should("have.prop", "beforeReload", true)

        cy.get(".articleCardReadMore").click({ multiple: true, force: true })

        cy.window().should("not.have.prop", "beforeReload")
    })
})
