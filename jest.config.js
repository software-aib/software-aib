module.exports = {
    verbose: true,
    setupFilesAfterEnv: ["./jest.setup.js"],
    testEnvironment: "jsdom",
    moduleNameMapper: {
        "^@/(.*)$": "<rootDir>/$1",
        "^~/(.*)$": "<rootDir>/$1",
        "^vue$": "vue/dist/vue.common.js",
    },
    moduleFileExtensions: ["ts", "js", "vue", "json"],
    transform: {
        "^.+\\.js$": "babel-jest",
        "^.+\\.vue$": "@vue/vue2-jest",
    },
    collectCoverage: true,
    collectCoverageFrom: [
        "<rootDir>/components/**/*.vue",
        "<rootDir>/pages/**/*.vue",
        "<rootDir>/layout/**/*.vue",
        "<rootDir>/store/**/*.vue",
    ],
    coverageDirectory: "<rootDir>/coverage/jest",
    testPathIgnorePatterns: ["<rootDir>/cypress/"],
    coverageReporters: ["json", "lcov"],
}
