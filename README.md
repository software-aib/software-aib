<p align="center">
  <a href="https://team-blog-io.netlify.app/"><img src="static/sqrs_logo.png"/></a>
</p>
<p align="center">
  <a href="https://www.figma.com/file/WygeSr2Gnea4LKxpl7d1PA/Blog?node-id=0%3A1">Figma</a> |
  <a href="https://www.canva.com/design/DAE2wLIjO_c/BssG0lVH0YKFjXRzmDK03g/view?utm_content=DAE2wLIjO_c&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink">Project Introduction</a> |
  <a href="https://www.canva.com/design/DAFANZSqvM8/dB-NurVpOx92NXPA-6W17w/edit?utm_content=DAFANZSqvM8&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton">Final Presentation</a>
</p>

<h3 align="center">
  In essence, the overall goal is to practice automated quality gate in CI/CD with a toy group project.
</h3>

<p align="center">
  SQR group project
</p>

<p align="center">
  <a href="https://gitlab.com/software-aib/software-aib/-/commits/dev"><img alt="pipeline status" src="https://gitlab.com/software-aib/software-aib/badges/dev/pipeline.svg" /></a>
  <a href="https://www.cypress.io/"><img alt="cypress" src="https://img.shields.io/badge/built%20with-nuxt%20and%20vuetify-purple.svg" /></a>
  <a href="https://www.cypress.io/"><img alt="cypress" src="https://img.shields.io/badge/tested%20with-Cypress-04C38E.svg" /></a>
  <a href="https://www.cypress.io/"><img alt="cypress" src="https://img.shields.io/badge/tested%20with-Jest-orange.svg" /></a>

</a><br />

</p>

## Our blog

<p align="center">
  <a href="https://team-blog-io.netlify.app/">
    <img alt="Why Cypress Video" src="static/blog.png" width="75%" height="75%" />
  </a>
</p>

## Installing

[![npm version](https://img.shields.io/badge/npm-8.7.0-bluegreen.svg?style=flat)](https://img.shields.io/badge/build-passing-brightgreen.svg?style=flat)

To get our project running for you, fork the repo and clone unto your local system then run the following in the project directory

```bash
$ npm install
$ npm run dev
```

## License

[![license](https://img.shields.io/badge/license-MIT-green.svg)](https://github.com/cypress-io/cypress/blob/master/LICENSE)

This project is licensed under the terms of the [MIT license](/LICENSE).
