module.exports = {
    root: true,
    env: {
        browser: true,
        node: true,
        "cypress/globals": true,
    },
    globals: {
        cy: true,
    },
    parserOptions: {
        parser: "@babel/eslint-parser",
        requireConfigFile: false,
        sourceType: "module",
        allowImportExportEverywhere: true,
    },
    extends: [
        "@nuxtjs",
        "plugin:nuxt/recommended",
        "prettier",
        "plugin:cypress/recommended",
    ],
    plugins: ["cypress", "istanbul"],
    // add your custom rules here
    rules: {
        "vue/multi-word-component-names": "off",
        "no-console": "off",
        quotes: ["error", "double", { avoidEscape: true }],
    },
}
